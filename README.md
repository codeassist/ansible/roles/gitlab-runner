|Branch|Status|
|------|:--------:|
|master|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/gitlab-runner/badges/master/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/gitlab-runner/commits/master)
|develop|[![pipeline status](https://gitlab.itsupportme.by/ansible-cm/roles/gitlab-runner/badges/develop/pipeline.svg)](https://gitlab.itsupportme.by/ansible-cm/roles/gitlab-runner/commits/develop)

## Ansible Role
### **_gitlab-runner_**

Register GitLab executor as system service on RHEL/CentOS, Debian/Ubuntu and Alpine Linux servers.

## Requirements

- Ansible 2.5 and higher

## Role Default Variables
```yaml
    # TODO
```

## Dependencies

None.

## License

MIT / BSD

## Author Information

This role was created in 2018 by ITSupportMe, LLC.
